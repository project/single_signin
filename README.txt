CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Single SignIn is a module that allows only one user can access the application at same time. When we login, it automatically logout from other devices or browsers.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/single_signin

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/single_signin



INSTALLATION
------------

 * Install the Single SignIn module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Clear the cache.

MAINTAINERS
-----------

Module created by:
 *  Asish Sajeev - https://www.drupal.org/u/asishsajeev